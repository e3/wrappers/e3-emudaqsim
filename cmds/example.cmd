require adcore
require admisc
require emudaqsim

require autosave
require busy
require calc

errlogInit(20000)
callbackSetQueueSize(15000)

#- load the instance definition
< $(E3_IOCSH_TOP)/instance.iocsh

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("PORT",                         "$(AMC_NAME)")
epicsEnvSet("MAX_SAMPLES",                  "100000")
#- AD plugin macros
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")


#- Create a EMU DAQ SIM driver
#- int emudaqsimConfig(const char *portName, const int maxChannels,
#-         int numSamples, int extraPorts, int maxBuffers, int maxMemory,
#-         int priority, int stackSize)
emudaqsimConfig("$(PORT)", 24, $(MAX_SAMPLES), 0, 0)
dbLoadRecords("$(emudaqsim_DB)/emudaqsim.template","P=$(PREFIX),R=,PORT=$(PORT),MAX_SAMPLES=$(MAX_SAMPLES)")

# #- 10x data channels
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=0, NAME=CH1")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=1, NAME=CH2")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=2, NAME=CH3")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=3, NAME=CH4")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=4, NAME=CH5")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=5, NAME=CH6")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=6, NAME=CH7")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=7, NAME=CH8")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=8, NAME=CH9")
iocshLoad("$(emudaqsim_DIR)channel.iocsh", "ADDR=9, NAME=CH10")

iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=10, NAME=CH11")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=11, NAME=CH12")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=12, NAME=CH13")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=13, NAME=CH14")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=14, NAME=CH15")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=15, NAME=CH16")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=16, NAME=CH17")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=17, NAME=CH18")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=18, NAME=CH19")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=19, NAME=CH20")

iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=20, NAME=CH21")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=21, NAME=CH22")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=22, NAME=CH23")
iocshLoad("$(emudaqsim_DIR)/channel.iocsh", "ADDR=23, NAME=CH24")

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

#- Configure autosave directories
set_requestfile_path("$(emudaqsim_DB)")
set_requestfile_path("$(asyn_DB)")
set_requestfile_path("$(adcore_DB)")
set_requestfile_path("./")

set_savefile_path("$(E3_IOCSH_TOP)/autosave")

#- apply default PV values (located in IOC_DIR)
# set_pass0_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")
# set_pass1_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")

#- apply runtime changed PV values (located in $(E3_IOCSH_TOP)/autosave)
set_pass0_restoreFile("info_positions.sav")
set_pass1_restoreFile("info_settings.sav")

# save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("$(autosave_DB)/save_restoreStatus.db","P=$(PREFIX)")

###############################################################################
iocInit
###############################################################################

#- build info_positions.req file from record info 'autosaveFields_pass0' field
#- build info_settings.req file from record info 'autosaveFields' field
makeAutosaveFiles("")
create_monitor_set("info_positions.req", 5)
#- save things every thirty seconds
create_monitor_set("info_settings.req", 30)

date
###############################################################################
